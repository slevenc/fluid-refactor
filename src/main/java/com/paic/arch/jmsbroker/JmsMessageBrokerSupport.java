package com.paic.arch.jmsbroker;

import com.paic.arch.jmsbroker.activemq.ActiveMQEmbeddedBrokerService;
import com.paic.arch.jmsbroker.jms.JMSQueueClient;
import com.paic.arch.jmsbroker.jms.JMSQueueMessageBroker;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.slf4j.Logger;

import javax.jms.ConnectionFactory;

import static org.slf4j.LoggerFactory.getLogger;

public class JmsMessageBrokerSupport {
    private static final Logger LOG = getLogger(JmsMessageBrokerSupport.class);
    MessageBrokerService messageBrokerService;
    private String brokerUrl;
    private MessageBroker messageBroker = null;

    private JmsMessageBrokerSupport(String aBrokerUrl) {
        brokerUrl = aBrokerUrl;
    }

    public static JmsMessageBrokerSupport createARunningEmbeddedBrokerOnAvailablePort() throws Exception {
        JmsMessageBrokerSupport jmsMessageBrokerSupport = new JmsMessageBrokerSupport(null);
        jmsMessageBrokerSupport.initServer(null);
        jmsMessageBrokerSupport.initBroker();
        return jmsMessageBrokerSupport;
    }

    public static JmsMessageBrokerSupport createARunningEmbeddedBrokerAt(String aBrokerUrl) throws Exception {
        LOG.debug("Creating a new broker at {}", aBrokerUrl);
        JmsMessageBrokerSupport jmsMessageBrokerSupport = new JmsMessageBrokerSupport(null);
        jmsMessageBrokerSupport.initServer(aBrokerUrl);
        jmsMessageBrokerSupport.initBroker();
        return jmsMessageBrokerSupport;
    }

    public static JmsMessageBrokerSupport bindToBrokerAtUrl(String aBrokerUrl) throws Exception {
        JmsMessageBrokerSupport jmsMessageBrokerSupport = new JmsMessageBrokerSupport(aBrokerUrl);
        jmsMessageBrokerSupport.initBroker();
        return jmsMessageBrokerSupport;
    }

    private void initServer(String aBrokerUrl) {
        if (aBrokerUrl != null) {
            messageBrokerService = ActiveMQEmbeddedBrokerService.createARunningEmbeddedBrokerOnUrl(aBrokerUrl);
        } else {
            messageBrokerService = ActiveMQEmbeddedBrokerService.createARunningEmbeddedBrokerOnAvailablePort();
        }
        brokerUrl = messageBrokerService.getBrokerUrl();
    }

    private void initBroker() {
        messageBroker = bindToActiveMqBrokerAt(brokerUrl);
    }

    public void stopTheRunningBroker() throws Exception {
        if (messageBrokerService == null) {
            throw new IllegalStateException("Cannot stop the broker from this API: " +
                    "perhaps it was started independently from this utility");
        }
        messageBrokerService.stopTheRunningBroker();
    }

    public final JmsMessageBrokerSupport andThen() {
        return this;
    }

    public final String getBrokerUrl() {
        return brokerUrl;
    }

    public JmsMessageBrokerSupport sendATextMessageToDestinationAt(String aDestinationName, final String aMessageToSend) {
        messageBroker.sendTheMessage(aMessageToSend).to(aDestinationName);
        return this;
    }

    public String retrieveASingleMessageFromTheDestination(String aDestinationName) {
        try {
            return messageBroker.waitForAMessageOn(aDestinationName);
        } catch (com.paic.arch.jmsbroker.NoMessageReceivedException e) {
            throw new NoMessageReceivedException(e.getMessage());
        }
    }

    public String retrieveASingleMessageFromTheDestination(String aDestinationName, final int aTimeout) {
        try {
            return messageBroker.waitForAMessageOn(aDestinationName, aTimeout);
        } catch (com.paic.arch.jmsbroker.NoMessageReceivedException e) {
            throw new NoMessageReceivedException(e.getMessage());
        }
    }



    public long getEnqueuedMessageCountAt(String aDestinationName) throws Exception {
        return messageBrokerService.getEnqueuedMessageCountAt(aDestinationName);
    }

    public boolean isEmptyQueueAt(String aDestinationName) throws Exception {
        return messageBrokerService.isEmptyAt(aDestinationName);
    }

    private MessageBroker bindToActiveMqBrokerAt(String url) {
        ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(url);
        JMSQueueClient jmsQueueClient = new JMSQueueClient();
        jmsQueueClient.setConnectionFactory(connectionFactory);
        JMSQueueMessageBroker broker = new JMSQueueMessageBroker();
        broker.setJmsQueueClient(jmsQueueClient);
        return broker;
    }

    public class NoMessageReceivedException extends RuntimeException {
        public NoMessageReceivedException(String reason) {
            super(reason);
        }
    }
}
