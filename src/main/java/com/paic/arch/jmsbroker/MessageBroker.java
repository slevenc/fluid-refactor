package com.paic.arch.jmsbroker;

/**
 * A fluid api to <code>MessageSender</code> and <code>MessageReciever</code>
 * Created by slevenc on 2018/2/8.
 */
public interface MessageBroker {

    MessageBroker and();

    MessageBroker sendTheMessage(String message);

    MessageBroker to(String aDestinationName);

    MessageBroker andThen();

    String waitForAMessageOn(String aDestinationName);

    String waitForAMessageOn(String aDestinationName, long timeout);
}
