package com.paic.arch.jmsbroker;

/**
 * A Reciever Timeout Exception
 * Created by slevenc on 2018/2/8.
 */
public class NoMessageReceivedException extends RuntimeException {
    public NoMessageReceivedException(String reason) {
        super(reason);
    }


}