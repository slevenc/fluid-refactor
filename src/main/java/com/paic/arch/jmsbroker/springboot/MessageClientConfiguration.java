package com.paic.arch.jmsbroker.springboot;

import com.paic.arch.jmsbroker.MessageBroker;
import com.paic.arch.jmsbroker.jms.JMSQueueClient;
import com.paic.arch.jmsbroker.jms.JMSQueueMessageBroker;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.jms.ConnectionFactory;

/**
 * Created by slevenc on 2018/2/8.
 */
@Configuration
public class MessageClientConfiguration {

    @Value("${jms.scheme}")
    private String scheme;
    @Value("${jms.host}")
    private String host;
    @Value("${jms.port}")
    private int port;

    @Bean
    public ConnectionFactory getConnectionFactory() {
        String url = scheme + "://" + host + ":" + port;
        ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(url);
        return connectionFactory;
    }

    @Bean
    public MessageBroker getMessageBroker(ConnectionFactory connectionFactory) {
        JMSQueueClient client = new JMSQueueClient();
        client.setConnectionFactory(connectionFactory);
        JMSQueueMessageBroker messageBroker = new JMSQueueMessageBroker();
        messageBroker.setJmsQueueClient(client);
        return messageBroker;
    }


    public void setScheme(String scheme) {
        this.scheme = scheme;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public void setPort(int port) {
        this.port = port;
    }
}
