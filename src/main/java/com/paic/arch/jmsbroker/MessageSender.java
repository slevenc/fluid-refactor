package com.paic.arch.jmsbroker;

/**
 * Message Sender
 * Created by slevenc on 2018/2/8.
 */
public interface MessageSender {
    /**
     * Send a Message to a named Destination
     *
     * @param aDestination
     * @param message
     */
    void sendATextMessageToDestinationAt(String aDestination, String message);
}
