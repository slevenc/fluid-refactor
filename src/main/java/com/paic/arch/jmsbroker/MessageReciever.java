package com.paic.arch.jmsbroker;

/**
 * Message Reciever
 * Created by slevenc on 2018/2/8.
 */
public interface MessageReciever {
    /**
     * Receives the next message that arrives,timeout <= 0 mean use default timeout
     *
     * @param aDestinationName
     * @param timeout  time out in millisecond
     * @return
     * @throws NoMessageReceivedException
     */
    String retrieveASingleMessageFromTheDestination(String aDestinationName, long timeout) throws NoMessageReceivedException;
}
