package com.paic.arch.jmsbroker.activemq;

import com.paic.arch.jmsbroker.MessageBrokerService;
import com.paic.arch.jmsbroker.SocketFinder;
import org.apache.activemq.broker.Broker;
import org.apache.activemq.broker.BrokerService;
import org.apache.activemq.broker.region.DestinationStatistics;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;


/**
 * The <code>ActiveMQEmbeddedBrokerService</code> is a Embedded ActiveMQ Impl for
 * MessageBrokerService
 * Other Implementations is support
 * Created by slevenc on 2018/2/8.
 */
public class ActiveMQEmbeddedBrokerService implements MessageBrokerService {


    private static final Logger LOG = LoggerFactory.getLogger(ActiveMQEmbeddedBrokerService.class);
    private static final String DEFAULT_BROKER_URL_PREFIX = "tcp://localhost:";
    private String brokerUrl = null;
    private BrokerService brokerService;

    private ActiveMQEmbeddedBrokerService() {

    }

    public static MessageBrokerService createARunningEmbeddedBrokerOnAvailablePort() {
        return createARunningEmbeddedBrokerAt(DEFAULT_BROKER_URL_PREFIX + SocketFinder.findNextAvailablePortBetween(41616, 50000));
    }

    public static MessageBrokerService createARunningEmbeddedBrokerOnUrl(String url) {
        return createARunningEmbeddedBrokerAt(url);
    }

    private static MessageBrokerService createARunningEmbeddedBrokerAt(String aBrokerUrl) {
        LOG.debug("Creating a new broker at {}", aBrokerUrl);
        ActiveMQEmbeddedBrokerService broker = new ActiveMQEmbeddedBrokerService();
        broker.brokerUrl = aBrokerUrl;
        try {
            broker.createEmbeddedBroker();
            broker.startEmbeddedBroker();
        } catch (Exception e) {
            throw new RuntimeException("create broker fail", e);
        }
        return broker;
    }


    private void createEmbeddedBroker() throws Exception {
        brokerService = new BrokerService();
        brokerService.setPersistent(false);
        brokerService.addConnector(brokerUrl);
    }

    private void startEmbeddedBroker() throws Exception {
        brokerService.start();
    }

    @Override
    public String getBrokerUrl() {
        return brokerUrl;
    }

    @Override
    public void stopTheRunningBroker() {
        isAlive();
        try {
            brokerService.stop();
            brokerService.waitUntilStopped();
        } catch (Exception e) {
            LOG.warn("Failed to stop brokerService at []", brokerUrl);
            throw new IllegalStateException(e);
        }
    }


    @Override
    public long getEnqueuedMessageCountAt(String aDestinationName) {
        try {
            return getDestinationStatisticsFor(aDestinationName).getMessages().getCount();
        } catch (Exception e) {
            throw new RuntimeException("get statistics fail", e);
        }
    }

    @Override
    public boolean isEmptyAt(String aDestinationName) {
        return getEnqueuedMessageCountAt(aDestinationName) == 0;
    }

    private DestinationStatistics getDestinationStatisticsFor(String aDestinationName) throws Exception {
        Broker regionBroker = brokerService.getRegionBroker();
        for (org.apache.activemq.broker.region.Destination destination : regionBroker.getDestinationMap().values()) {
            if (destination.getName().equals(aDestinationName)) {
                return destination.getDestinationStatistics();
            }
        }
        throw new IllegalStateException(String.format("Destination %s does not exist on broker at %s", aDestinationName, brokerUrl));
    }

    @Override
    public void deleteAllMessage() {
        isAlive();
        try {
            brokerService.deleteAllMessages();
        } catch (IOException e) {
            throw new RuntimeException("Cannot delete message", e);
        }
    }


    private void isAlive() {
        if (brokerService == null) {
            throw new IllegalStateException("Cannot stop the broker from this API: " +
                    "perhaps it was started independently from this utility");
        }
    }
}
