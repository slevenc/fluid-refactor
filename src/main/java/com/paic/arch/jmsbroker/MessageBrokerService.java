package com.paic.arch.jmsbroker;

import java.io.IOException;

/**
 * Provider a message broker service
 * Created by slevenc on 2018/2/8.
 */
public interface MessageBrokerService {
    /**
     * @return url of this broker service
     */
    String getBrokerUrl();

    /**
     * stop broker service
     */
    void stopTheRunningBroker();

    /**
     * count enqueued message
     *
     * @param aDestinationName
     * @return
     * @throws Exception
     */
    long getEnqueuedMessageCountAt(String aDestinationName);

    /**
     * @param aDestinationName
     * @return
     * @throws Exception
     */
    boolean isEmptyAt(String aDestinationName);

    /**
     * clear all message
     *
     * @throws IOException
     */
    void deleteAllMessage();
}
