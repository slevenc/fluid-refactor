package com.paic.arch.jmsbroker.jms;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jms.*;
import java.lang.IllegalStateException;

/**
 * Created by slevenc on 2018/2/8.
 */
public abstract class JMSExecutor {
    private static final Logger LOG = LoggerFactory.getLogger(JMSExecutor.class);

    public String execute(ConnectionFactory connectionFactory) {
        return executeCallbackAgainstRemoteBroker(connectionFactory);
    }

    protected abstract String getDestnationName();

    protected abstract String perform(Session session, Queue queue) throws JMSException;

    private String executeCallbackAgainstConnection(Connection aConnection) {
        Session session = null;
        try {
            session = aConnection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            Queue queue = session.createQueue(getDestnationName());
            return perform(session, queue);
        } catch (JMSException jmse) {
            LOG.error("Failed to create session on connection {}", aConnection);
            throw new IllegalStateException(jmse);
        } finally {
            if (session != null) {
                try {
                    session.close();
                } catch (JMSException jmse) {
                    LOG.warn("Failed to close session {}", session);
                    throw new IllegalStateException(jmse);
                }
            }
        }
    }

    private String executeCallbackAgainstRemoteBroker(ConnectionFactory connectionFactory) {
        Connection connection = null;
        String returnValue = "";
        try {
            connection = connectionFactory.createConnection();
            connection.start();
            return executeCallbackAgainstConnection(connection);
        } catch (JMSException jmse) {
            LOG.error("failed to create connection to {}", connectionFactory);
            throw new IllegalStateException(jmse);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (JMSException jmse) {
                    LOG.warn("Failed to close connection to client at []", connectionFactory);
                    throw new IllegalStateException(jmse);
                }
            }
        }
    }
}
