package com.paic.arch.jmsbroker.jms;

import com.paic.arch.jmsbroker.MessageBroker;

/**
 * Created by slevenc on 2018/2/8.
 */
public class JMSQueueMessageBroker implements MessageBroker {
    private JMSQueueClient jmsQueueClient;
    private String textMessage = null;

    @Override
    public MessageBroker and() {
        return this;
    }

    @Override
    public MessageBroker sendTheMessage(String message) {
        textMessage = message;
        return this;
    }

    @Override
    public MessageBroker to(String aDestinationName) {
        if (textMessage != null) {
            jmsQueueClient.sendATextMessageToDestinationAt(aDestinationName, textMessage);
        } else {
            throw new IllegalStateException("no message content");
        }
        return this;
    }

    @Override
    public MessageBroker andThen() {
        return this;
    }

    @Override
    public String waitForAMessageOn(String aDestinationName) {
        return jmsQueueClient.retrieveASingleMessageFromTheDestination(aDestinationName, 0);
    }

    @Override
    public String waitForAMessageOn(String aDestinationName, long timeout) {
        return jmsQueueClient.retrieveASingleMessageFromTheDestination(aDestinationName, timeout);
    }


    public void setJmsQueueClient(JMSQueueClient jmsQueueClient) {
        this.jmsQueueClient = jmsQueueClient;
    }
}
