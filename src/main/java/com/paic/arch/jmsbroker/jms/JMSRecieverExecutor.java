package com.paic.arch.jmsbroker.jms;

import com.paic.arch.jmsbroker.NoMessageReceivedException;

import javax.jms.*;

/**
 * Created by slevenc on 2018/2/8.
 */
public class JMSRecieverExecutor extends JMSExecutor {

    private String destnationName;

    private long timeout;


    public JMSRecieverExecutor(String destnationName, long timeout) {
        this.destnationName = destnationName;
        this.timeout = timeout;
    }

    @Override
    protected String getDestnationName() {
        return this.destnationName;
    }

    @Override
    protected String perform(Session session, Queue queue) throws JMSException {
        MessageConsumer consumer = session.createConsumer(queue);
        Message message = consumer.receive(timeout);
        if (message == null) {
            throw new NoMessageReceivedException(String.format("No messages received from the client within the %d timeout", timeout));
        }
        consumer.close();
        return ((TextMessage) message).getText();
    }
}
