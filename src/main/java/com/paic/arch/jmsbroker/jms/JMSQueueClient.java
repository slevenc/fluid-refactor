package com.paic.arch.jmsbroker.jms;

import com.paic.arch.jmsbroker.MessageReciever;
import com.paic.arch.jmsbroker.MessageSender;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jms.ConnectionFactory;
import java.util.concurrent.TimeUnit;


/**
 * Created by slevenc on 2018/2/8.
 */
public class JMSQueueClient implements MessageSender, MessageReciever {
    public static final long DEFAULT_RECEIVE_TIMEOUT = TimeUnit.SECONDS.toMillis(10);
    private static final Logger LOG = LoggerFactory.getLogger(JMSQueueClient.class);
    private long recieverTimeout = DEFAULT_RECEIVE_TIMEOUT;

    private ConnectionFactory connectionFactory;

    public void setConnectionFactory(ConnectionFactory connectionFactory) {
        this.connectionFactory = connectionFactory;
    }

    @Override
    public void sendATextMessageToDestinationAt(String aDestination, String message) {
        new JMSSenderExecutor(aDestination, message).execute(connectionFactory);
    }

    @Override
    public String retrieveASingleMessageFromTheDestination(String aDestinationName, long timeout) {
        if (timeout <= 0) {
            timeout = recieverTimeout;
        }
        return new JMSRecieverExecutor(aDestinationName, timeout).execute(connectionFactory);
    }
}
