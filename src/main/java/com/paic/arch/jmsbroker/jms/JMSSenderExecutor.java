package com.paic.arch.jmsbroker.jms;

import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;

/**
 * Created by slevenc on 2018/2/8.
 */
public class JMSSenderExecutor extends JMSExecutor {

    private String destnationName;

    private String message;

    public JMSSenderExecutor(String destnationName, String message) {
        this.destnationName = destnationName;
        this.message = message;
    }

    @Override
    protected String getDestnationName() {
        return this.destnationName;
    }

    @Override
    protected String perform(Session session, Queue queue) throws JMSException {
        MessageProducer producer = session.createProducer(queue);
        producer.send(session.createTextMessage(message));
        producer.close();
        return "";
    }
}
