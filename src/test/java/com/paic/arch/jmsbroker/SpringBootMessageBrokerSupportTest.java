package com.paic.arch.jmsbroker;

import com.paic.arch.jmsbroker.springboot.SpringBootMainApplication;
import org.junit.AfterClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Unit Test use Spring-boot
 * Created by slevenc on 2018/2/8.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = SpringBootMainApplication.class)
public class SpringBootMessageBrokerSupportTest {

    public static final String TEST_QUEUE = "MY_TEST_QUEUE";
    private static MessageBrokerService MESSAGE_BROKER_SERVICE;
    @Autowired
    private MessageBroker messageBroker;

    @AfterClass
    public static void teardown() throws Exception {
        MESSAGE_BROKER_SERVICE.stopTheRunningBroker();
    }

    @Test
    public void sendsMessagesToTheRunningBroker() throws Exception {
        String content = UUID.randomUUID().toString();
        messageBroker.sendTheMessage(content).to(TEST_QUEUE);
        long messageCount = MESSAGE_BROKER_SERVICE.getEnqueuedMessageCountAt(TEST_QUEUE);
        assertThat(messageCount).isEqualTo(1);
    }

    @Test
    public void readsMessagesPreviouslyWrittenToAQueue() throws Exception {
        MESSAGE_BROKER_SERVICE.deleteAllMessage();
        String content = UUID.randomUUID().toString();
        String receieveContent = messageBroker.sendTheMessage(content).to(TEST_QUEUE)
                .waitForAMessageOn(TEST_QUEUE);
        assertThat(receieveContent).isEqualTo(content);
    }

    @Test(expected = NoMessageReceivedException.class)
    public void throwsExceptionWhenNoMessagesReceivedInTimeout() throws Exception {
        MESSAGE_BROKER_SERVICE.deleteAllMessage();
        messageBroker.waitForAMessageOn(TEST_QUEUE, 1);
    }

    public void setMessageBroker(MessageBroker messageBroker) {
        this.messageBroker = messageBroker;
    }

    @Autowired
    public void setJmsMessageBroker(MessageBrokerService messageBrokerService) {
        SpringBootMessageBrokerSupportTest.MESSAGE_BROKER_SERVICE = messageBrokerService;
    }
}
