package com.paic.arch.jmsbroker;

import com.paic.arch.jmsbroker.activemq.ActiveMQEmbeddedBrokerService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by slevenc on 2018/2/8.
 */
@Configuration
public class TestConfiguration {


    @Value("${jms.scheme}")
    private String scheme;
    @Value("${jms.host}")
    private String host;
    @Value("${jms.port}")
    private int port;


    @Bean
    public MessageBrokerService getJmsMessageBroker() throws Exception {
        return ActiveMQEmbeddedBrokerService.createARunningEmbeddedBrokerOnUrl(scheme + "://" + host + ":" + port);
    }

    public void setScheme(String scheme) {
        this.scheme = scheme;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public void setPort(int port) {
        this.port = port;
    }


}
