package com.paic.arch.jmsbroker;

import com.paic.arch.jmsbroker.activemq.ActiveMQEmbeddedBrokerService;
import com.paic.arch.jmsbroker.jms.JMSQueueClient;
import com.paic.arch.jmsbroker.jms.JMSQueueMessageBroker;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.jms.ConnectionFactory;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

public class NewMessageBrokerSupportTest {

    public static final String TEST_QUEUE = "MY_TEST_QUEUE";
    private static MessageBrokerService ACTIVEMQ_EMBEDDED_BROKER;
    private static String REMOTE_BROKER_URL;

    @BeforeClass
    public static void setup() throws Exception {
        ACTIVEMQ_EMBEDDED_BROKER = ActiveMQEmbeddedBrokerService.createARunningEmbeddedBrokerOnAvailablePort();
        REMOTE_BROKER_URL = ACTIVEMQ_EMBEDDED_BROKER.getBrokerUrl();
    }

    @AfterClass
    public static void teardown() throws Exception {
        ACTIVEMQ_EMBEDDED_BROKER.stopTheRunningBroker();
    }

    @Test
    public void sendsMessagesToTheRunningBroker() throws Exception {
        String content = UUID.randomUUID().toString();
        bindToActiveMqBrokerAt(REMOTE_BROKER_URL)
                .and().sendTheMessage(content).to(TEST_QUEUE);
        long messageCount = ACTIVEMQ_EMBEDDED_BROKER.getEnqueuedMessageCountAt(TEST_QUEUE);
        assertThat(messageCount).isEqualTo(1);
    }

    @Test
    public void readsMessagesPreviouslyWrittenToAQueue() throws Exception {
        ACTIVEMQ_EMBEDDED_BROKER.deleteAllMessage();
        String content = UUID.randomUUID().toString();
        String recieverContent = bindToActiveMqBrokerAt(REMOTE_BROKER_URL)
                .and().sendTheMessage(content).to(TEST_QUEUE)
                .andThen().waitForAMessageOn(TEST_QUEUE);
        assertThat(recieverContent).isEqualTo(content);
    }

    @Test(expected = NoMessageReceivedException.class)
    public void throwsExceptionWhenNoMessagesReceivedInTimeout() throws Exception {
        ACTIVEMQ_EMBEDDED_BROKER.deleteAllMessage();
        bindToActiveMqBrokerAt(REMOTE_BROKER_URL)
                .waitForAMessageOn(TEST_QUEUE, 1);
    }

    private MessageBroker bindToActiveMqBrokerAt(String url) {
        ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(url);
        JMSQueueClient jmsQueueClient = new JMSQueueClient();
        jmsQueueClient.setConnectionFactory(connectionFactory);
        JMSQueueMessageBroker broker = new JMSQueueMessageBroker();
        broker.setJmsQueueClient(jmsQueueClient);
        return broker;
    }


}